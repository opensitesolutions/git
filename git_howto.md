# Table of Contents

- [SSH key](#ssh_key)
- [Configure your GIT profile](#git_profile)
- Creating your workspace
- Creating your code directory
- Adding and committing content to your repository
- Branches
  - Creating branches
  - Delete local branch
  - Delete remote branch
  - List branches
  - Sync remote and local branches
  - Forking
- Git reset
- Pull Request
- Appendix

## SSH Key <a name="ssh_key"></a>

Generate your ssh-keys
```ssh-keys```

### Configure you GIT profile< name="git_profile"></a>

Substitute your name and email below.

git config --global user.name "Clarence Mills"
git config --global user.email "clarence.mills@scotiabank.com"

## Creating your workspace

Adding content to your local repository is done using the “git add” command

1. Create A Readme File
    -  touch README.md
    -  echo "Your Full name Ansible Repository" > README.md

2. Stage your code, add the README.md to the staging
    - git add README.md

Once you have added your content, the next step is to commit your content to your local repository using the git command git commit -m “some explanation on what is being committed”

3. Commit files the README.md into your local repository.
    -  git commit -m "Initial Commit”

4. Sync local repository with remote repository.
    - Push contents of your local repository to you remote bitbucket created above.
    -  git push origin master

For a complete list of GIT commands and how to use either use git help or Bitbucket great online Git documentation.

